Feature: Change Language Functionality

  @Smoke
  Scenario: Change the Language to English
    Given the user is on the Introduction page
    When the user clicks on the "Lang change" button
    Then the user is redirected to the Language page 
    And the user selects the "English" language and clicks on the "Submit" button
    Then the user is redirected to the Introduction page again and verifies the presence of the "Get Started" button
   