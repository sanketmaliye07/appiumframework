package dwManagement;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;
import org.testng.annotations.BeforeSuite;
import Utilities.AndroidUtilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;

public class DWManager {

	protected AndroidDriver driver;	
	private Properties properties;
	protected AndroidUtilities utilities;  
	
	@BeforeSuite
	public void setUp() throws IOException {
	    System.out.println("Setting up...");

	    try {
	        loadProperties();

	        UiAutomator2Options options = new UiAutomator2Options();
	        options.setCapability("deviceName", properties.getProperty("DEVICE.NAME"));
	        options.setCapability("app", properties.getProperty("APP.PATH.DW"));
	        URL appiumServerUrl = new URL(properties.getProperty("APPIUM.SERVER.URL"));
	        setDriver(new AndroidDriver(appiumServerUrl, options));
	        getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	        System.out.println("AndroidDriver initialized successfully.");
	    } catch (IOException e) {
	        e.printStackTrace();  
	    }
	}

	//@AfterSuite
	public void tearDown() {
		if (getDriver() != null) {
			getDriver().quit();
		}
	}

	private void loadProperties() throws IOException {
		properties = new Properties();
		FileInputStream fis = new FileInputStream("src/main/resources/config.properties");
		properties.load(fis);
		fis.close(); 
		String appPath = properties.getProperty("APP.PATH.DW");
		if (appPath.startsWith(properties.getProperty("APP.PATH.DW"))) {
			String frameworkDir = System.getProperty("user.dir");
			properties.setProperty("APP.PATH.DW", frameworkDir + "/" + appPath);
		}
	}
	
	public AndroidDriver getDriver() {
		return driver;
	}

	public void setDriver(AndroidDriver driver) {
		this.driver = driver;
	}
}
