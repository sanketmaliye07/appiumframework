/*
 * package dWStepDefinitions;
 * 
 * import java.io.IOException;
 * 
 * import Utilities.AssertionsUtils; import Utilities.WaitUtilities; import
 * dWPageRepo.DWFunctionLibrary; import dWPageRepo.HomePage; import
 * dWPageRepo.IntroductionPage; import dWPageRepo.LanguagePage; import
 * dWPageRepo.ProfilePage; import dWPageRepo.SignUpPage; import
 * dwManagement.DWManager; import io.cucumber.java.en.And; import
 * io.cucumber.java.en.Given; import io.cucumber.java.en.Then; import
 * io.cucumber.java.en.When;
 * 
 * public class LoginStepDef extends DWManager { WaitUtilities wait = new
 * WaitUtilities(getDriver()); DWFunctionLibrary chefkartfunctionlibrary;
 * AssertionsUtils assertions; LanguagePage languagepage = new LanguagePage();
 * 
 * public LoginStepDef() throws IOException { super.setUp();
 * chefkartfunctionlibrary = new DWFunctionLibrary(driver); assertions = new
 * AssertionsUtils(driver); }
 * 
 * @Given("the user is on the Introduction page") public void
 * the_user_is_on_the_introduction_page() { wait.sleepInSeconds(5);
 * assertions.assertElementPresent(IntroductionPage.ShurukareButton); }
 * 
 * @When("the user clicks on the {string} button") public void
 * the_user_clicks_on_the_button(String string) {
 * chefkartfunctionlibrary.clickOnChangeLanguageButton(); }
 * 
 * @Then("the user is redirected to the Language page") public void
 * the_user_is_redirected_to_the_language_page() {
 * assertions.assertElementPresent(LanguagePage.ApniBhashaChuneText);
 * assertions.assertElementPresent(LanguagePage.JamaKareButton); }
 * 
 * @Then("the user selects the {string} language and clicks on the {string} button"
 * ) public void the_user_selects_the_language_and_clicks_on_the_button(String
 * string, String string2) {
 * chefkartfunctionlibrary.clickOnEnglishLanguageButton();
 * assertions.assertElementPresent(LanguagePage.SubmitButton);
 * chefkartfunctionlibrary.clickOnSubmitButton(); }
 * 
 * @Then("the user is redirected to the Introduction page again and verifies the presence of the {string} button"
 * ) public void the_user_is_redirected_to_the_introduction_page_again(String
 * string) { assertions.assertElementPresent(IntroductionPage.GetStartedButton);
 * tearDown(); }
 * 
 * @And("the user clicks on the {string} button on introduction page") public
 * void the_user_clicks_on_the_get_started_button(String string) {
 * chefkartfunctionlibrary.clickOnGetStartedButton(); }
 * 
 * @Given("the user change the language to {string}") public void
 * the_user_change_the_language_to(String string) {
 * chefkartfunctionlibrary.ChangeLanguage(); }
 * 
 * @Then("the user enters the mobile number {string} and click on \"Continue\" button"
 * ) public void the_user_enters_the_mobile_number(String mobilenumber) {
 * chefkartfunctionlibrary.enterMobileNumber(mobilenumber);
 * chefkartfunctionlibrary.clickOnContinueButton(); }
 * 
 * @Then("the user enters the otp {string}") public void
 * the_user_enters_the_otp(String otp) { chefkartfunctionlibrary.enterOTP(otp);
 * }
 * 
 * @Then("the user logged in successfully") public void
 * the_user_logged_in_successfully() {
 * assertions.assertElementPresent(HomePage.ProfileNameText);
 * assertions.assertElementPresent(HomePage.RegistrationSteps); tearDown(); }
 * 
 * @Then("then user login to application") public void
 * then_user_login_to_application() { chefkartfunctionlibrary.Login(); }
 * 
 * @Then("user scroll down to {string} button and clicks it") public void
 * user_scroll_down_to_button_and_clicks_it(String string) {
 * chefkartfunctionlibrary.scrollandClickProfileButton(); }
 * 
 * @Then("user is on profile page") public void user_is_on_profile_page() {
 * assertions.assertElementPresent(ProfilePage.ProfileLabel); }
 * 
 * @Then("user clicks {string} button on profile page") public void
 * user_clicks_button_on_profile_page(String string) {
 * chefkartfunctionlibrary.scrollandClickLogoutButton(); }
 * 
 * @Given("user logged out successfully") public void
 * user_logged_out_successfully() {
 * assertions.assertElementPresent(SignUpPage.BhashaBadleinButton);
 * assertions.assertElementPresent(SignUpPage.EnterYourMobileNumberText);
 * tearDown(); } }
 */