package dWCases;

import org.testng.annotations.Test;

import dWPageRepo.DWFunctionLibrary;
import dwManagement.DWManager;

public class SignInCases extends DWManager{

	@Test(priority=1)
	public void LaunchApp() {
		 
	}
	
	@Test(priority=2)
	public void SignInWithCredentials() {
		DWFunctionLibrary fl = new DWFunctionLibrary(getDriver());
		fl.SignIn();	 
	}
}
