package dWPageRepo;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.WebElement;
import Utilities.AndroidUtilities;
import Utilities.AssertionsUtils;
import Utilities.SeleniumUtils;
import Utilities.WaitUtilities;
import io.appium.java_client.android.AndroidDriver;

public class DWFunctionLibrary {

	AssertionsUtils assertions;
	AndroidUtilities androidutils;
	WaitUtilities waitutils;
	AndroidDriver driver;
	SeleniumUtils seleniumutils;	

	public DWFunctionLibrary(AndroidDriver driver) {
	    this.driver = driver;
	    this.seleniumutils = new SeleniumUtils(driver);
	    this.assertions = new AssertionsUtils(driver);
	    this.androidutils = new AndroidUtilities(driver);
	    this.waitutils = new WaitUtilities(driver);
	}

	public DWFunctionLibrary ChangeLanguageInUAEPass() {
		List<WebElement> els1 = driver.findElements(IntroductionPage.English);
		if (!els1.isEmpty()) {
			els1.get(0).click();
		} else {
			System.out.println("Element with accessibility id 'English' not found");
		}
		return this;
	}
	
	public DWFunctionLibrary ClickOnEnglishButton() {
		waitutils.waitForElementPresent(IntroductionPage.EnglishDW, Duration.ofSeconds(10));
		seleniumutils.clickElement(IntroductionPage.EnglishDW);
		return this;
	}
	public DWFunctionLibrary ClickOnQAButton() {
		waitutils.waitForElementPresent(HomePage.QA, Duration.ofSeconds(10));
		seleniumutils.clickElement(HomePage.QA);
		return this;
	}
	public DWFunctionLibrary ClickOnSkipButton() {
		waitutils.waitForElementPresent(HomePage.Skip, Duration.ofSeconds(10));
		seleniumutils.clickElement(HomePage.Skip);
		return this;
	}
	public DWFunctionLibrary ClickOnSignInWithUAEPASSButton() {
		waitutils.waitForElementPresent(SignInPage.SigninwithUAEPASS, Duration.ofSeconds(10));
		seleniumutils.clickElement(SignInPage.SigninwithUAEPASS);
		return this;
	}
	public DWFunctionLibrary SignIn() {
		ClickOnEnglishButton();
		ClickOnQAButton();
		ClickOnSkipButton();
		ClickOnSignInWithUAEPASSButton();
		return this;
	}

}
