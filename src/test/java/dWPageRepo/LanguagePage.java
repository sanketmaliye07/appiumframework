package dWPageRepo;

import org.openqa.selenium.By;

import io.appium.java_client.AppiumBy;

public class LanguagePage {
	
	public static By EnglishLanguage = AppiumBy.accessibilityId("A\nEnglish");
	
	public static By HindiLanguage = AppiumBy.accessibilityId("अ\nहिंदी");
	
	public static By BanglaLanguage = AppiumBy.accessibilityId("বা\nবাংলা");
	
	public static By JamaKareButton = AppiumBy.accessibilityId("जमा करें");
	
	public static By ApniBhashaChuneText = AppiumBy.accessibilityId("अपनी भाषा चुनें");
	
	public static By LanguageLogo = AppiumBy.className("android.widget.ImageView");
	
	public static By SubmitButton = AppiumBy.accessibilityId("Submit");

}
